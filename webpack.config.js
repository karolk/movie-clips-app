const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


const isProduction = process.env.NODE_ENV === 'production';

const cssDev = ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'];

const cssProd = ExtractTextPlugin.extract({
     fallback: 'style-loader',
     use: ['css-loader', 'postcss-loader', 'sass-loader'],
     publicPath: '/dist'
});

const cssConfig = isProduction ? cssProd : cssDev;


module.exports = {
  entry: './src/js/main.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      {
        test: /\.scss$/,
        use: cssConfig
      },
      {
        test: /\.(png|jpg|gif|mp4|webm)$/,
        use: [
          'srcset-loader',
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'img/',
              publicPath: ''
            }
          }
        ]
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          attrs: ['img:src', 'img:srcset', 'source:srcset']
        }
      },
      {
           test: /\.(woff2?|svg)$/,
           use: [
                {
                     loader: 'url-loader',
                     options: {
                          limit: 10000
                     }
                }
           ]
      },
      {
        test: /\.(ttf|eot|woff2?)$/,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      },
    ]
  },
  devServer: {
     port: 6080
  },
  plugins: [
    
    new CopyWebpackPlugin([
       {
           context: './src/img/',
           from: 'sprite-icon.svg',
           to: './img'
       },
       {
          context: './src/img/default_img/',
          from: '*.png',
          to: './img'
       },
       {
          context: './src/img/favicons/',
          from: '*',
          to: './img/favicons'
       }
    ]),

     new HtmlWebpackPlugin({
          minify: {
            collapseWhitespace: true
          },
          template: './src/index.html'
     }),

    new ExtractTextPlugin({
      filename: 'style.css',
      disable: !isProduction,
      allChunks: true
    })

  ]
};
