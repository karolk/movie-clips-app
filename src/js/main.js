import 'babel-polyfill';
import 'whatwg-fetch';

// Import style
import '../scss/main.scss';

//Import Modules
import './app';