export const DataModel = (function() {
     
     const movie_genres = [];

     let watch_list = [];

     const status = (response) => {
          if(response.status >= 200 && response.status < 400) {
               return Promise.resolve(response);
          } else {
               return Promise.reject({
                    status: response.status,
                    statusText: response.statusText
               });
          }
     }

     const fetchMovieData = (url) => {
          return fetch(url)
                    .then(status)
                    .then(response => response.json())
                    .catch(error => alert(Error(`Request filed HTTP status is ${error.status} ${error.statusText}`)))
     };
     
     return {

          fetchMovieGenres() {
               const genres = 'https://api.themoviedb.org/3/genre/movie/list?api_key=4aa3a7b345e94044e381eb3b661f136c&language=en-US';
               fetchMovieData(genres).then(genre => movie_genres.push(...genre.genres));
          },

          fetchMovies(category, input) {
               
               const movie_category = `https://api.themoviedb.org/3/movie/${category}?api_key=4aa3a7b345e94044e381eb3b661f136c&page=1&region=US`;
               
               const search_movie = `https://api.themoviedb.org/3/search/movie?api_key=4aa3a7b345e94044e381eb3b661f136c&query=${input}&page=1&include_adult=false`;

               const movies = category !== 'search' ? movie_category : search_movie;
               return fetchMovieData(movies);
          },

          fetchMovieDetail(id) {
               const detail = `https://api.themoviedb.org/3/movie/${id}?api_key=4aa3a7b345e94044e381eb3b661f136c&append_to_response=videos,credits,release_dates`;
               return fetchMovieData(detail);
          },

          findGenres(movies, genres) {
               // Map through film genres to create ids array
               // Map through ids array to get the elements then match genre id with film id
               return movies
                    .map(movie => movie.genre_ids)
                    .map((ids) => {
                         return ids.map((index) => {
                              for(let value of movie_genres.values()) {
                                   if(value.id === index) return value.name;
                              }
                         })
                    });
          },

          getWatchList() {
               return watch_list;
          },

          addToStorage(movie) {
               const new_movie = Object.assign({}, movie);
               const movie_data = watch_list.find(el => el.id === new_movie.id);
               if(!movie_data) watch_list.push(new_movie);
               localStorage.setItem('watchList', JSON.stringify(watch_list));
          },

          getStorage() {
               return watch_list = JSON.parse(localStorage.getItem('watchList')) || [];
          }
          
     }

})();