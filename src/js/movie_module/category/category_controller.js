import { DataModel } from '../data_model';
import { CategoryUI } from './category_ui_view'; 

export const CategoryController = (function(Data, CtgUI) {
     
     const DOM = CtgUI.getDOM();

     const getGenres = () => Data.fetchMovieGenres();

     const getStorageData = () => Data.getStorage();

     const crtlCategory = async(category = 'now_playing', input = '') => {
          // Get movie data
          const movies = await Data.fetchMovies(category, input).then(movie => movie.results);

          if(movies.length > 0) {
               //Find geners
               const genres = await Data.findGenres(movies);
               // Get movie data to the ui
               const set_movie_UI = await CtgUI.setMovie(category, movies, genres);
               const add_animation_movie = await CtgUI.aniamteMovieItem(category);
          } else {
               const show_message = await CtgUI.showMessage(category);
          }
          
     };

     const currentView = (category) => {
           // Clear movies containers
           const clear_movie_containers = CtgUI.clearMovieContainer(category);
           // Show chosen section
           const show_section_UI = CtgUI.showSection(category);
     };

     const changeView = function(category) {
           // Change current view
           const change_view = currentView(category);
           // Add active class to nav link
           const nav_link_active = CtgUI.setNavActiveLink(category);
           return category;
     };

     const getCategory = function(e) {
            e.preventDefault();
            // Get category
            const category = this.getAttribute('href').replace(/\#/g, '');
            const view = changeView(category);
            // Get movies
            const get_movie = crtlCategory(view);
     };

     const crtlSearch = function(e) {
          e.preventDefault();
          // Get input values
          const user_input = CtgUI.getInputValues().inputValue;
          const category = CtgUI.getInputValues().inputName;

          if(user_input) {
              // Change view
              const change_view = currentView(category);
              const remove_active_nav = CtgUI.removeNavActiveLink();
              const closeMobileMenu = CtgUI.colseMobileMenu();
              // Find movies
              const find_movie = crtlCategory(category, user_input);
              // Clear from
              const clear_form = this.reset();
          }
     };

     const dataWatchList = (watch_list) => {
            // Get movies
            const list_movie = Data.getWatchList();
            if(list_movie.length > 0) {
                  // Get movie data to the ui
                  const set_movie_UI = CategoryUI.setWatchListMovie(list_movie, watch_list);
                  const add_animation_movie = CategoryUI.aniamteMovieItem(watch_list);
            } else {
                  const show_message = CategoryUI.showMessage();
            }
      };

     const watchList = function(e) {
            e.preventDefault();
            // Get category
            const watch_list = this.getAttribute('href').replace(/\#/g, '');
            // Change view
            const view = changeView(watch_list);
            // Get movies
            const get_watlist_movie = dataWatchList(watch_list);
     };

     const crtlMenu = () => CtgUI.mobileMenu();

     const setUpEventListeners = () => {
          const nav_links_array = Array.from(document.querySelectorAll(DOM.navLinks));
          nav_links_array.forEach(el => el.addEventListener('click', getCategory));
          // Search movie  
          document.querySelector(DOM.searchForm).addEventListener('submit', crtlSearch);
          // Open watchlist
          document.querySelector(DOM.navWatchList).addEventListener('click', watchList);
          // Open Mobile Menu
          document.querySelector(DOM.mobileMenu).addEventListener('click', crtlMenu);
     };  

      return {
            init() {
                  getStorageData();
                  getGenres();
                  crtlCategory();
                  setUpEventListeners();
            }
      }
})(DataModel, CategoryUI);