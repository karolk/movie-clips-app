export const CategoryUI = (function() {

     const DOMstrings = {
          navLinks: '.js--nav_category',
          navWatchList: '.js--nav_watchlist',
          searchForm: '.js--search',
          movieContainer: '.js--movie-container',
          section: '.js--section',
          movieDetail: '.js--movie-detail',
          movieDetailBox: '.js--movie-detail-overlay-content',
          mobileMenu: '.js--mobile-menu',
          mainContent: '.js--main',
          sidebarMenu: '.js--sidebar',
          movieDetailActiveClass: 'movie-detail--active',
          movieActiveClass: 'movie--active',
          movieVoteHidden: 'movie__vote--hidden',
          navLinksActiveClass: 'nav__link--active',
          sectionActiveClass: 'section--active',
          mobileMenuActiveClass: 'mobile-active'
     };

     const checkPoster = poster => poster !== null && poster !== 'lack' ? `https://image.tmdb.org/t/p/w342/${poster}` : 'img/themoviedb-poster.png' ;

     const matchGenre = (genres, index) => {
          return genres[index].map((genre, number) => {
               return genre !== undefined ? `<span class="movie__genre">${genre}</span>` : '';
          }).join('');
     };

     const checkVotes = vote => parseInt(vote) === 0 ? DOMstrings.movieVoteHidden : '';

     return {

          getInputValues() {
               const form = document.querySelector(DOMstrings.searchForm);
               return {
                    inputValue: form.querySelector('[type=text]').value.trim().replace(/\s+/g, '%20'),
                    inputName: form.querySelector('[type=text]').name

               }
          },

          setMovie(category, movies, genres) {
               const movieContainer = document.querySelector(`#${category} > ${DOMstrings.movieContainer}`);
               movieContainer.innerHTML = movies.map((movie, index) => {
                    return `
                    <div class="movie js--movie">
                              <figure class="movie__poster">
                                   <a href="#${movie.id}" class="movie__link js--movie-link">
                                        <img src="${checkPoster(movie.poster_path)}" alt="Poster Image" class="movie__img">
                                   </a>
                                   <figcaption class="movie__vote">
                                        <span class="movie__vote-average ${checkVotes(movie.vote_average)}">${movie.vote_average}</span>
                                   </figcaption>       
                              </figure>
                         <div class="movie__info">
                              <h4 class="heading-fourth">${movie.title}</h4>
                              <div class="movie__genres">
                                   ${matchGenre(genres, index)}
                              </div>
                         </div>
                    </div>
                    `;
               }).join('');
          },

          setWatchListMovie(movies, category) {
               const movieContainer = document.querySelector(`#${category} > ${DOMstrings.movieContainer}`);
               movieContainer.innerHTML = movies.map((movie, index) => {
                    return `
                    <div class="movie js--movie">
                              <figure class="movie__poster">
                                   <a href="#${movie.id}" class="movie__link js--movie-link">
                                        <img src="${checkPoster(movie.poster)}" alt="Poster Image" class="movie__img">
                                   </a>
                                   <figcaption class="movie__vote">
                                        <span class="movie__vote-average ${checkVotes(movie.rate)}">${movie.rate}</span>
                                   </figcaption>       
                              </figure>
                         <div class="movie__info">
                              <h4 class="heading-fourth">${movie.title}</h4>
                         </div>
                    </div>
                    `;
               }).join('');
          },

          aniamteMovieItem(category) {
               const movies = Array.from(document.querySelectorAll(`.js--movie`));
               movies.forEach((el, i) => {
                    setTimeout(() => el.classList.add(`${DOMstrings.movieActiveClass}`), 300 * ( i * 0.5));
               });
          },

          showSection(category) {
               const remove_detail = this.removeMovieDetails();
               const sections = Array.from(document.querySelectorAll(DOMstrings.section));
               // Remove class active
               sections.forEach(el => el.classList.remove(DOMstrings.sectionActiveClass));
               // Add class active to current category section
               document.querySelector(`#${category}`).classList.add(DOMstrings.sectionActiveClass)
               // Open mobile menu
               this.mobileMenu('close');
          },

          clearMovieContainer() {
               Array.from(document.querySelectorAll(DOMstrings.movieContainer)).forEach(el => el.innerHTML = ''); 
          },

          removeMovieDetails() {
               document.querySelector(DOMstrings.movieDetail).classList.remove(DOMstrings.movieDetailActiveClass);
               document.querySelector(DOMstrings.movieDetailBox).innerHTML = '';
          },

          removeNavActiveLink() {
               Array.from(document.querySelectorAll(DOMstrings.navLinks))
                    .forEach(el => el.classList.remove(DOMstrings.navLinksActiveClass));
               document.querySelector(DOMstrings.navWatchList).classList.remove(DOMstrings.navLinksActiveClass);
          },

          setNavActiveLink(category) {
               this.removeMovieDetails();
               this.removeNavActiveLink();
               document.querySelector(`a[href='#${category}']`).classList.add(DOMstrings.navLinksActiveClass);
          },

          showMessage(category = 'watch_list') {
               document.querySelector(`#${category} > ${DOMstrings.movieContainer}`).innerHTML = `
               <div class="message">
                    <span class="message__text">${category !== 'watch_list' ? 'There are no movies that matched your query.': 'You do not have any movies'}</span>
               </div>
               `;
          },

          colseMobileMenu() {
               this.mobileMenu('close');
               this.removeMovieDetails()
          },

          mobileMenu(action = 'open') {
               const elements = Array.from(document.querySelectorAll(`${DOMstrings.sidebarMenu}, ${DOMstrings.mainContent}, ${DOMstrings.mobileMenu}`));
               elements.forEach(el => 
                    action ? el.classList.toggle(DOMstrings.mobileMenuActiveClass): el.classList.remove(DOMstrings.mobileMenuActiveClass));
               this.removeMovieDetails()
          },

          getDOM() {
               return DOMstrings;
          }
     }
     
})();