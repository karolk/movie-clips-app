import { DataModel } from '../data_model';
import { MovieUI } from './movie_ui';

export const MovieController = (function(Data, UIMovie) {

     const DOM = UIMovie.getDOM();

     const movieDetail = async(id) => {
          const movie_details = await Data.fetchMovieDetail(id).then(detail => detail);
          // Get movie data to the ui
          const show_movie_details = await UIMovie.showMovieDetails();
          const set_movie_details = await UIMovie.setMovieDetail(movie_details);
     };

     const crtlMovieDetail = function(e) {
          const movie_link = e.target.parentNode;
          // if it is not a movie link , skip this
          if(!movie_link.matches(DOM.movieLink)) return;
          const movie_id = movie_link.getAttribute('href').replace(/#/g, '');
          // Get movie details
          const details = movieDetail(movie_id);
     };

     const addToWatchList = (e) => {
          // if it is not a btn-add , skip this
          if(!e.target.matches(DOM.btnAddWatchList)) return;
          // Get the movie id
          const movie_data = e.target.dataset
          // Add movie id to data
          const storage = Data.addToStorage(movie_data);
          
     };

     const playTrailer = (e) => {
          // if it is not a btn-play , skip this
          if(!e.target.matches(DOM.btnPlay)) return;
          const key = e.target.dataset.key;
          const add_trailer = MovieUI.addTrailer(key);
     };

     const closeMovieDetail = () => UIMovie.removeMovieDetails();

     const setUpEventListeners = () => {
          const containers = Array.from(document.querySelectorAll(DOM.movieContainer));
          containers.forEach(el => el.addEventListener('click', crtlMovieDetail));
          // Close movie detail
          document.querySelector(DOM.btnClose).addEventListener('click', closeMovieDetail);
          // Play trailer
          document.querySelector(DOM.movieDetailBox).addEventListener('click', playTrailer);
          // Add to watchList
          document.querySelector(DOM.movieDetailBox).addEventListener('click', addToWatchList);
     };

     return {
          init() {
               setUpEventListeners();
          }
     }
})(DataModel, MovieUI);