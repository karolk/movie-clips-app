export const MovieUI = (function() {

     const DOMstrings = {
          movie: '.js--movie',
          movieLink: '.js--movie-link',
          movieContainer: '.js--movie-container',
          movieDetail: '.js--movie-detail',
          movieDetailBox: '.js--movie-detail-overlay-content',
          movieTrailer: '.js--movie-detail-trailer',
          movieDetailActiveClass: 'movie-detail--active',
          btnClose:'.js--btn-close',
          btnPlay: '.js--btn-play',
          btnAddWatchList: '.js--btn-add-watchlist'
     };

     const video_type = 'Trailer';
     const video_clip = 'Clip';
     const job = 'Director';

     const backdrop = (path) => path !== null ? `https://image.tmdb.org/t/p/w1280${path}` : 'img/backdrop.png';

     const checkPoster = poster => poster !== null ? `<img src="https://image.tmdb.org/t/p/w300/${poster}" alt="Poster Img" class="movie-detail__poster">` : '' ;

     const video_id = (movie) => {
          const video = movie.videos.results.find((el) => {
               if(el.type === video_type || el.type === video_clip) return el;
          });
          return video ? 
          `<div class="movie-detail__play">
               <svg class="movie-detail__play-icon">
                    <use xlink:href="img/sprite-icon.svg#icon-ondemand_videoicon-"></use>
               </svg>
               <div class="movie-detail__play-box">
                    <button class="btn-play js--btn-play" data-key="${video.key}">Play Trailer</button>
               </div>
          </div>`:
          `${checkPoster(movie.poster_path)}`;
     };

     const video_id_mobile = (movie) => {
          const video = movie.videos.results.find(el => el.type === video_type);
          return video ? 
          `<a href="https://www.youtube.com/embed/${video.key}?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0"  class="btn-play btn-play--mobile" target="blank">Play Trailer</a>` : '';
     }

     const runtime = (time) => {
          const min_hour = 60;
          let [hours, minutes] = (time / min_hour).toFixed(2).split('.');
          return `
               <span class="movie-detail__runtime">
                    <span class="movie-detail__hours">${minutes < min_hour ? hours : parseInt(hours) + 1}h</span>
                    <span class="movie-detail__minutes">${minutes < min_hour ? minutes : parseInt(minutes) - min_hour}m</span>
               </span>`;
     };

     const match_director = (crew) => {
          const director = crew.find(person => person.job === job);
          return director ? `<p class="movie-detail__director">Directed by: <span class="movie-detail__director-name">${director.name}</span></p>` : '';
     };
     
     const rates = (rate) => rate !== 0 ? `<span class="movie-detail__rate"><span class="movie-detail__vote">${rate}</span> / 10 </span>`: '';

     const release = (date) => date.split(/\-/g)[0];

     return {

          showMovieDetails() {
               document.querySelector(DOMstrings.movieDetail).classList.add(DOMstrings.movieDetailActiveClass);
          },

          removeMovieDetails() {
               document.querySelector(DOMstrings.movieDetail).classList.remove(DOMstrings.movieDetailActiveClass);
               document.querySelector(DOMstrings.movieDetailBox).innerHTML = '';
          },

          setMovieDetail(movie) {
               document.querySelector(DOMstrings.movieDetailBox).innerHTML = `
                    <img src="${backdrop(movie.backdrop_path)}" alt="Backdrop Image" class="movie-detail__bg">
                              
                    <div class="movie-detail__trailer js--movie-detail-trailer">
                         ${video_id(movie)}
                    </div>
                    <div class="movie-detail__content">
                         <h3 class="movie-detail__heading">
                              ${movie.original_title}
                         </h3>
                         <div class="movie-detail__info">
                              ${rates(movie.vote_average)}

                              <span class="movie-detail__year">
                                   ${release(movie.release_date)}
                              </span>

                              ${movie.runtime !== null && movie.runtime !== 0  ? runtime(movie.runtime) : ''}
                         </div>

                         ${movie.overview !== '' ? `<p class="movie-detail__overview">${movie.overview}</p>` : ''}

                         ${match_director(movie.credits.crew)}

                         <div class="movie-detail__add">

                              <button class="btn-add js--btn-add-watchlist" data-id=${movie.id} data-title="${movie.original_title}" data-poster=${movie.poster_path !== null ? movie.poster_path : 'lack'} data-rate=${movie.vote_average} >
                                   <svg class="btn-add__icon">
                                        <use xlink:href="img/sprite-icon.svg#icon-addicon-"></use>
                                   </svg>
                                   Add to watchlist
                              </button>
                              
                              ${video_id_mobile(movie)}
                         </div>
                    </div>
               `;
          },

          addTrailer(key) {
               document.querySelector(DOMstrings.movieTrailer).innerHTML = 
               `<iframe class="movie-detail__video"  src="https://www.youtube.com/embed/${key}?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" autoplay allowfullscreen></iframe>`
          },

          getDOM() {
               return DOMstrings;
          }
     }
     
})();