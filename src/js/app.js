// Import module crtl
import { CategoryController } from './movie_module/category/category_controller';
import { MovieController } from './movie_module/movie/movie_controller';

CategoryController.init();
MovieController.init();