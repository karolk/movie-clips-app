# Movie Clips App

### Live [Movie Clips App](https://movie-clips-app-js.netlify.com)

![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Fmovie-clips-trailers.png?alt=media&token=a13bf89d-5d45-49b5-b2fe-95e7ff2b0f39)

### Description

"Movie Clip Trailers" is an application to view the 20 latest, highest rated and themost popular movies. In addition, the user can search detail information for his favorite movies and waht is more, watch the trailer of the selected movie. Each user can add the selected movie to their favorites clicking by on the button "Add to watchlist".

### Project structure and implementation of the project have been implemented and designed by Karol Kozer.

### The project was made (for personal purposes and portfolio) based on graphic project Movie Aplication UI Dribble

### The project includes:

Webapack 2 , ES6 / ES7, Babel , Sass , Themoviedb API

### Themoviedb API

The Movie Database (TMDb) is a community built movie and TV database. Every piece of data has been added by our amazing community dating back to 2008. TMDb's strong international focus and breadth of data is largely unmatched and something we're incredibly proud of. Put simply, we live and breathe community and that's precisely what makes us different.

[Themoviedb](https://www.themoviedb.org/)

### Run project

Require [Node](https://nodejs.org/en/)

Install the dependencies:

```
npm install
```

Run dev server:

```
npm run dev
```

Localhost:
`http://localhost:6080/`
